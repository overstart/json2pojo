public class Config {
  static class Server {
    private Integer port;

    public Integer getPort() {
      return this.port;
    }

    public void setPort(Integer port) {
      this.port = port;
    }

    private String keystorePath;

    public String getKeystorePath() {
      return this.keystorePath;
    }

    public void setKeystorePath(String keystorePath) {
      this.keystorePath = keystorePath;
    }
  }

  private Server server;

  public Server getServer() {
    return this.server;
  }

  public void setServer(Server server) {
    this.server = server;
  }

  static class Datasource {
    private String host;

    public String getHost() {
      return this.host;
    }

    public void setHost(String host) {
      this.host = host;
    }

    private Integer port;

    public Integer getPort() {
      return this.port;
    }

    public void setPort(Integer port) {
      this.port = port;
    }

    private String username;

    public String getUsername() {
      return this.username;
    }

    public void setUsername(String username) {
      this.username = username;
    }

    private String password;

    public String getPassword() {
      return this.password;
    }

    public void setPassword(String password) {
      this.password = password;
    }

    private String database;

    public String getDatabase() {
      return this.database;
    }

    public void setDatabase(String database) {
      this.database = database;
    }

    private Integer maxPoolSize;

    public Integer getMaxPoolSize() {
      return this.maxPoolSize;
    }

    public void setMaxPoolSize(Integer maxPoolSize) {
      this.maxPoolSize = maxPoolSize;
    }
  }

  private Datasource datasource;

  public Datasource getDatasource() {
    return this.datasource;
  }

  public void setDatasource(Datasource datasource) {
    this.datasource = datasource;
  }
}
