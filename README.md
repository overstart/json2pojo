# json2pojo
> 一款快速为json文件生成java代码的工具

### 使用方法
```bash
# 用默认的演示配置文件生成演示代码
./gradlew run
# 通过参数指定json文件地址， java文件夹地址， 包名， 空格隔开
./gradlew run --args="/path/to/json /path/to/javadir/ javaPackageName"
```